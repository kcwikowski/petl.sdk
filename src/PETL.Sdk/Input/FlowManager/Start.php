<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Input\FlowManager;

use PETL\Sdk\Input;

class Start extends Input
{
    /**
     * @var string
     */
    protected $flow;
    /**
     * @var array
     */
    protected $options;
    /**
     * @var string
     */
    protected $preset;
    /**
     * @var string
     */
    protected $region;
    /**
     * Internal parameter.
     * Setting value manually might result in failed request.
     *
     * @var string
     */
    protected $executionContext = '';

    /**
     * @return string
     */
    public function getFlow()
    {
        return $this->flow;
    }

    /**
     * @param string $flow
     * @return static
     */
    public function setFlow($flow)
    {
        $this->flow = $flow;

        return $this;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        if (is_null($this->options)) {
            $this->options = [];
        }

        return $this->options;
    }

    /**
     * @param array $options
     * @return static
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return string
     */
    public function getPreset()
    {
        return $this->preset;
    }

    /**
     * @param string $preset
     * @return static
     */
    public function setPreset($preset)
    {
        $this->preset = $preset;

        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     * @return static
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return string
     */
    public function getExecutionContext()
    {
        return $this->executionContext;
    }

    /**
     * @param string $executionContext
     * @return static
     */
    public function setExecutionContext($executionContext)
    {
        $this->executionContext = $executionContext;

        return $this;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        return
            is_string($this->getFlow())
            && is_array($this->getOptions())
            && (
                is_string($this->getPreset())
                || is_null($this->getPreset())
            )
            && (
                is_string($this->getRegion())
                || is_null($this->getRegion())
            );
    }
}