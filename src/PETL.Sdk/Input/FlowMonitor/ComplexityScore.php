<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Input\FlowMonitor;

use PETL\Sdk\Input\FlowMonitor;

class ComplexityScore extends FlowMonitor
{
    /**
     * @var int
     */
    protected $totalType;
    /**
     * @var string
     */
    protected $timeTag;

    /**
     * @return int
     */
    public function getTotalType()
    {
        return $this->totalType;
    }

    /**
     * @param int $totalType
     * @return static
     */
    public function setTotalType($totalType)
    {
        $this->totalType = $totalType;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimeTag()
    {
        return $this->timeTag;
    }

    /**
     * @param string $timeTag
     * @return static
     */
    public function setTimeTag($timeTag)
    {
        $this->timeTag = $timeTag;

        return $this;
    }
}