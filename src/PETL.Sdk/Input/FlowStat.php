<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Input;

use PETL\Sdk\Input;

class FlowStat extends Input
{
    /**
     * @var string
     */
    protected $flow;
    /**
     * @var string
     */
    protected $type;
    /**
     * @var string
     */
    protected $timeTag;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return static
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getTimeTag()
    {
        return $this->timeTag;
    }

    /**
     * @param string $timeTag
     * @return static
     */
    public function setTimeTag($timeTag)
    {
        $this->timeTag = $timeTag;

        return $this;
    }

    /**
     * @return string
     */
    public function getFlow()
    {
        return $this->flow;
    }

    /**
     * @param string $flow
     * @return static
     */
    public function setFlow($flow)
    {
        $this->flow = $flow;

        return $this;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        return
            is_string($this->getType())
            && (is_string($this->getTimeTag()) || is_null($this->getTimeTag()));
    }
}