<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Input;

use PETL\Sdk\Input;

class FlowMonitor extends Input
{
    /**
     * @var string
     */
    protected $flow;
    /**
     * @var string
     */
    protected $flowInstance;

    /**
     * @return string
     */
    public function getFlow()
    {
        return $this->flow;
    }

    /**
     * @param string $flow
     * @return static
     */
    public function setFlow($flow)
    {
        $this->flow = $flow;

        return $this;
    }

    /**
     * @return string
     */
    public function getFlowInstance()
    {
        return $this->flowInstance;
    }

    /**
     * @param string $flowInstance
     * @return static
     */
    public function setFlowInstance($flowInstance)
    {
        $this->flowInstance = $flowInstance;

        return $this;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        return true;
    }
}