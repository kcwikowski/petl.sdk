<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk;

use PETL\Standard\Common\JsonSerializableTrait;
use PETL\Standard\Common\MessengerInterface;
use PETL\Standard\Common\MessengerTrait;

class Result implements MessengerInterface, \JsonSerializable
{
    use MessengerTrait, JsonSerializableTrait;
    /**
     * @var bool
     */
    protected $successful;

    /**
     * @return bool
     */
    public function isSuccessful()
    {
        return $this->successful;
    }

    /**
     * @param bool $successful
     * @return static
     */
    public function setSuccessful($successful)
    {
        $this->successful = $successful;

        return $this;
    }
}