<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Authentication;

class KeyGenerator implements KeyGeneratorInterface
{
	/**
	 * @var string[]
	 */
	protected static $stacks = [];

    /**
     * @param $length
     * @param bool $returnAsString
     * @return array|null|string
     */
	public static function generateRandomBytes($length, $returnAsString = true)
	{
		if (!is_numeric($length) || $length < 1) {
			return null;
		}
		if ($returnAsString) {
			$random = '';

			for ($i = 0; $i < $length; $i++) {
				$random .= chr(mt_rand() % 0b11111111);
			}
		} else {
			$random = [];

			for ($i = 0; $i < $length; $i++) {
				$random[] = mt_rand() % 0b11111111;
			}
		}

		return $random;
	}

	/**
	 * @param int $length
	 * @param int $mode
	 * @return string
	 */
	public static function generateRandomString($length, $mode = self::MODE_ALL)
	{
		if (!is_numeric($length) || $length < 1) {
			return null;
		}

		$stack        = self::getStack($mode);
		$stackLength  = strlen($stack);
		$randomString = '';

		for ($i = 0; $i < $length; $i++) {
			$randomString .= $stack[mt_rand() % $stackLength];
		}

		return $randomString;
	}

	/**
	 * @param int $stackMode
	 * @return string
	 */
	protected static function getStack($stackMode)
	{

		if (!array_key_exists($stackMode, self::getStacks())) {
			self::$stacks[$stackMode] = self::createStack($stackMode);
		}

		return self::getStacks()[$stackMode];
	}

	/**
	 * @return \string[]
	 */
	public static function getStacks()
	{

		return self::$stacks;
	}

	/**
	 * @param \string[] $stacks
	 */
	public static function setStacks($stacks)
	{

		self::$stacks = $stacks;
	}

    /**
     * @param $stackMode
     * @return string
     */
	protected static function createStack($stackMode)
	{

		$stack = '';

		foreach (
			[
				self::MODE_CHARS_LOW     => self::CHARS_LOW,
				self::MODE_CHARS_UP      => self::CHARS_UP,
				self::MODE_CHARS_SPECIAL => self::CHARS_SPECIAL,
				self::MODE_DIGITS        => self::DIGITS,
				self::MODE_HEX           => self::HEX,
			] as $mode => $characters) {
			if ($stackMode & $mode) {
				$stack .= $characters;
			}
		}

		return $stack;
	}
}