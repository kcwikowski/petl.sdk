<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Authentication;

use PETL\Standard\Authentication\IdentityAwareTrait;
use PETL\Standard\Authentication\KSCBlockAwareInterface;
use PETL\Standard\Authentication\KSCBlockAwareTrait;
use PETL\Standard\Authentication\SecureData;
use PETL\Standard\Common\Configuration;
use PETL\Standard\Common\Message;
use PETL\Standard\Common\MessengerInterface;
use PETL\Standard\Common\MessengerTrait;
use PETL\Standard\Common\Util;

/**
 * Class Stateless
 * @package PETL\Sdk\Authentication
 */
class Stateless implements MessengerInterface, KSCBlockAwareInterface
{
    use MessengerTrait, IdentityAwareTrait, KSCBlockAwareTrait;
    const DEFAULT_HASH_ALGO         = 'sha512';
    const USE_RANDOM_KEY            = -1;
    const ENCRYPTION_CIPHER_DEFAULT = 'AES-256-CBC';
    /**
     * @var string
     */
    protected $hashAlgo;
    /**
     * @var bool
     */
    protected $encryption;
    /**
     * @var string
     */
    protected $encryptionCipher;
    /**
     * @var bool
     */
    protected $compression;

    /**
     * Stateless constructor.
     * @param array $configuration
     */
    public function __construct(array $configuration = [])
    {

        Configuration::apply($this, $configuration);
    }

    /**
     * @return bool
     */
    public function isCompression()
    {
        return $this->compression;
    }

    /**
     * @param bool $compression
     * @return self
     */
    public function setCompression($compression)
    {
        $this->compression = $compression;

        return $this;
    }

    /**
     * @return string
     */
    public function getEncryptionCipher()
    {

        if (is_null($this->encryptionCipher)) {
            $this->encryptionCipher = self::ENCRYPTION_CIPHER_DEFAULT;
        }

        return $this->encryptionCipher;
    }

    /**
     * @param $encryptionCipher
     * @return $this
     */
    public function setEncryptionCipher($encryptionCipher)
    {

        $this->encryptionCipher = $encryptionCipher;

        return $this;
    }

    /**
     * @param array $data
     * @param int $key
     * @return SecureData
     */
    public function sign($data = [], $key = self::USE_RANDOM_KEY)
    {

        if (self::USE_RANDOM_KEY === $key) {
            $key = $this->getKsc()->getRandomKey();
        }
        $salt = $timestamp = null;
        $hash = $this->generateHash($key, $data, $timestamp);

        if ($this->isEncryption()) {
            $salt = KeyGenerator::generateRandomString(32);
            $data = $this->encryptData($key, $data, $salt);
        }

        return
            (new SecureData())
                ->setIdentity($this->getIdentity())
                ->setData($data)
                ->setKey($key)
                ->setTime($timestamp)
                ->setHash($hash)
                ->setSalt($salt)
                ->setEncrypted($this->isEncryption())
                ->setCompressed($this->isCompression())
                ->setSigned(true);
    }

    /**
     * @param      $key
     * @param null $data
     * @param null $timestamp
     * @return string
     */
    public function generateHash($key, $data = null, &$timestamp = null)
    {

        if (is_null($timestamp)) {
            $timestamp = time();
        }

        $secret = $this->getKsc()->getSecret($key);
        $seed   = $secret . $this->getDataSignature($data) . $this->getIdentity() . $key . $timestamp;
        $hash   = hash($this->getHashAlgo(), $seed);

        return $hash;
    }

    /**
     * @return boolean
     */
    public function isEncryption()
    {

        return $this->encryption;
    }

    /**
     * @param $encryption
     * @return $this
     */
    public function setEncryption($encryption)
    {

        $this->encryption = $encryption;

        return $this;
    }

    /**
     * @param $data
     * @return string
     */
    public function getDataSignature($data)
    {

        if (!is_scalar($data)) {
            $data = json_encode($data);
        }

        $signature = hash($this->getHashAlgo(), $data);

        return $signature;
    }

    /**
     * @return string
     */
    public function getHashAlgo()
    {

        if (is_null($this->hashAlgo)) {
            $this->hashAlgo = self::DEFAULT_HASH_ALGO;
        }

        return $this->hashAlgo;
    }

    /**
     * @param $hashAlgo
     * @return $this
     * @throws \Exception
     */
    public function setHashAlgo($hashAlgo)
    {

        if (!in_array($hashAlgo, hash_algos())) {
            throw new \Exception('Invalid hash algorithm.');
        }

        $this->hashAlgo = $hashAlgo;

        return $this;
    }

    /**
     * @param SecureData $signedData
     * @param null $outputData
     * @return bool
     */
    public function verify(SecureData $signedData, &$outputData = null)
    {

        if (!$signedData->preValidate()) {

            $this->addMessage(
                Message::error('Request is missing authentication components')
            );

            return false;
        }

        $data       = $signedData->getData();
        $timestamp  = $signedData->getTime();
        $validation = true;

        if ($signedData->isEncrypted()) {
            $data =
                $this->decryptData(
                    $signedData->getKey(),
                    $data,
                    $signedData->getSalt()
                );
        }

        if (!(
            $signedData->getHash() ===
            $this->generateHash(
                $signedData->getKey(),
                $data,
                $timestamp
            )
        )) {
            $this->addMessage('Invalid signature');
            $validation = false;
        }

        if ($validation) {
            $outputData = $data;
        }

        return $validation;
    }

    /**
     * @param      $key
     * @param      $data
     * @param null $salt
     * @return string
     */
    protected function encryptData($key, $data, $salt = null)
    {
        $ivLength      = openssl_cipher_iv_length($this->getEncryptionCipher());
        $iv            = KeyGenerator::generateRandomBytes($ivLength);
        $secret        = $this->getKsc()->getSecret($key) . $this->getIdentity() . $salt;
        $encryptedData =
            openssl_encrypt(
                Util::safeZip($data, !$this->isCompression()),
                $this->getEncryptionCipher(),
                hash(
                    'sha256',
                    $secret
                ),
                0,
                $iv
            );

        return bin2hex($iv) . $encryptedData;
    }

    /**
     * @param      $key
     * @param      $encryptedData
     * @param null $salt
     * @return mixed
     */
    protected function decryptData($key, $encryptedData, $salt = null)
    {
        $ivLength      = openssl_cipher_iv_length($this->getEncryptionCipher()) * 2;
        $iv            = hex2bin(substr($encryptedData, 0, $ivLength));
        $encryptedData = substr($encryptedData, $ivLength);
        $secret        = $this->getKsc()->getSecret($key) . $this->getIdentity() . $salt;
        $data          =
            Util::safeUnzip(
                openssl_decrypt(
                    $encryptedData,
                    $this->getEncryptionCipher(),
                    hash(
                        'sha256',
                        $secret
                    ),
                    0,
                    $iv
                )
            );

        return $data;
    }
}