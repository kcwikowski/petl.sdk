<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Authentication;

interface KeyGeneratorInterface
{
    const CHARS_LOW = 'qwertyuiopasdfghjklzxcvbnm';
    const CHARS_UP = 'QWERTYUIOPASDFGHJKLZXCVBNM';
    const CHARS_SPECIAL = '`~!@#$%^&*()_+<>?:|{}*-=[];,.';
    const DIGITS = '0123456789';
    const HEX = '0123456789ABCDEF';
    const MODE_ALL = 0b11110;
    const MODE_CHARS_LOW = 0b10000;
    const MODE_CHARS_UP = 0b01000;
    const MODE_CHARS_SPECIAL = 0b00100;
    const MODE_DIGITS = 0b00010;
    const MODE_HEX = 0b00001;
    const MODE_CHARS = self::MODE_CHARS_LOW | self::MODE_CHARS_UP;
    const MODE_PRINTABLE = self::MODE_CHARS | self::MODE_DIGITS;

    /**
     * @param $length
     * @param bool $returnAsString
     * @return array|null|string
     */
    public static function generateRandomBytes($length, $returnAsString = true);

    /**
     * @param int $length
     * @param int $mode
     * @return string
     */
    public static function generateRandomString($length, $mode = self::MODE_ALL);
}