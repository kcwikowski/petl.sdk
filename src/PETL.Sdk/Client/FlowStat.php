<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Client;

use PETL\Sdk\AbstractClient;
use PETL\Sdk\Input;
use PETL\Sdk\Result\Metric;

/**
 * Class FlowStat
 * @package PETL\Sdk
 *
 * @method Metric metric(array | Input\FlowStat $inputs)
 * @method Metric flowMetric(array | Input\FlowStat $inputs)
 */
class FlowStat extends AbstractClient
{
}