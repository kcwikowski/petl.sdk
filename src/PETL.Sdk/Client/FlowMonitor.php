<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Client;

use PETL\Sdk\AbstractClient;
use PETL\Sdk\Input;

/**
 * Class FlowMonitor
 * @package PETL\Sdk
 *
 * @method \PETL\Sdk\Result\FlowMonitor info(array|Input\FlowMonitor $inputs)
 * @method \PETL\Sdk\Result\FlowMonitor getMonthlyComplexityScore(array|Input\FlowMonitor\ComplexityScore $inputs)
 */
class FlowMonitor extends AbstractClient
{
}