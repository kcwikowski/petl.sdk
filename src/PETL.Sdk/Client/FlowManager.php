<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Client;

use PETL\Sdk\AbstractClient;
use PETL\Sdk\Input;
use PETL\Sdk\Result\FlowManager\Start;

/**
 * Class FlowManager
 * @package PETL\Sdk
 *
 * @method Start start(array|Input\FlowManager\Start $inputs)
 */
class FlowManager extends AbstractClient
{
}