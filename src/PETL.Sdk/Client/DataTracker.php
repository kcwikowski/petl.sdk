<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Client;

use PETL\Sdk\AbstractClient;

/**
 * Class DataTracker
 * @package PETL\Sdk\Client
 */
class DataTracker extends AbstractClient
{
}