<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk;

use PETL\Sdk\Authentication\Stateless;
use PETL\Standard\Common\Configuration;
use Zend\ServiceManager\AbstractFactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class ClientFactory
 * @package PETL\Sdk
 */
class ClientFactory implements AbstractFactoryInterface
{
    /**
     * @var bool
     */
    protected $initialized;
    /**
     * @var string
     */
    protected $classPrefix;
    /**
     * @var array
     */
    protected $clients;
    /**
     * @var string
     */
    protected $authOptionsPath;
    /**
     * @var AuthConfiguration
     */
    protected $globalAuthConfiguration;
    /**
     * @var string
     */
    protected $endpoint;

    public function canCreateServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        $this->initialize($serviceLocator);

        return
            (bool)$this->getClientName(
                $requestedName
            );
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     * @param $name
     * @param $requestedName
     * @return AbstractClient
     */
    public function createServiceWithName(ServiceLocatorInterface $serviceLocator, $name, $requestedName)
    {
        return
            $this->configure(
                $this->instantiate(
                    $this->getClientName($requestedName)
                )
            );
    }

    /**
     * @param ServiceLocatorInterface $sm
     * @return $this
     */
    public function initialize(ServiceLocatorInterface $sm)
    {

        if ($this->isInitialized()) {
            return $this;
        }

        $config = $sm->get('config');

        Configuration::apply(
            $this,
            Configuration::getOption(
                $config,
                $this->getFactoryOptionsPath(),
                []
            )
        );
        $this->setGlobalAuthConfiguration(
            new AuthConfiguration(
                Configuration::getOption(
                    $config,
                    $this->getAuthOptionsPath(),
                    []
                )
            )
        );
        $this->setInitialized(true);

        return $this;
    }

    /**
     * @return string
     */
    public function getFactoryOptionsPath()
    {
        return 'PETL.Sdk:Factory';
    }

    /**
     * @return bool
     */
    public function isInitialized()
    {
        return $this->initialized;
    }

    /**
     * @param bool $initialized
     * @return self
     */
    public function setInitialized($initialized)
    {
        $this->initialized = $initialized;

        return $this;
    }

    /**
     * @return string
     */
    public function getClassPrefix()
    {
        return $this->classPrefix;
    }

    /**
     * @param string $classPrefix
     * @return self
     */
    public function setClassPrefix($classPrefix)
    {
        $this->classPrefix = $classPrefix;

        return $this;
    }

    /**
     * @return array
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * @param array $clients
     * @return self
     */
    public function setClients($clients)
    {
        $this->clients = $clients;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthOptionsPath()
    {
        return $this->authOptionsPath;
    }

    /**
     * @param string $authOptionsPath
     * @return self
     */
    public function setAuthOptionsPath($authOptionsPath)
    {
        $this->authOptionsPath = $authOptionsPath;

        return $this;
    }

    /**
     * @return AuthConfiguration
     */
    public function getGlobalAuthConfiguration()
    {
        return $this->globalAuthConfiguration;
    }

    /**
     * @param AuthConfiguration $globalAuthConfiguration
     * @return self
     */
    public function setGlobalAuthConfiguration($globalAuthConfiguration)
    {
        $this->globalAuthConfiguration = $globalAuthConfiguration;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     * @return static
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    protected function getClientName($serviceName)
    {
        $clientName  = false;
        $serviceName = str_replace(
            '\\',
            '.',
            $serviceName
        );
        if (0 === strpos($serviceName, $this->getClassPrefix())) {
            $clientName =
                substr(
                    $serviceName,
                    strlen($this->getClassPrefix())
                );
        }

        return
            $clientName && array_key_exists($clientName, $this->getClients()) ?
                $clientName :
                false;
    }

    /**
     * @param $targetName
     * @return mixed
     */
    protected function getClassName($targetName)
    {
        return
            str_replace(
                '.',
                '\\',
                $this->getClassPrefix() . $targetName
            );
    }

    /**
     * @param AbstractClient $client
     * @return AbstractClient
     */
    private function configure(AbstractClient $client)
    {
        $clientConfigurationData = $this->getClients()[$client->getName()];
        $clientConfiguration     = new ClientConfiguration($clientConfigurationData);
        $auth                    =
            (new Stateless())
                ->setEncryption(true)
                ->setIdentity(
                    $this->getGlobalAuthConfiguration()->getIdentity()
                )
                ->setKsc(
                    $clientConfiguration->getKsc() ?: $this->getGlobalAuthConfiguration()->getKsc()
                );

        $client
            ->setAuth($auth)
            ->setRouteBase($clientConfiguration->getRouteBase())
            ->setEndpoint($clientConfiguration->getEndpoint() ?: $this->getEndpoint())
            ->setResultClass($clientConfiguration->getResultClass());

        if ($clientConfiguration->getMethods()) {
            foreach ($clientConfiguration->getMethods() as $methodName => $methodConfiguration) {
                $client->addMethod(
                    (new Method($methodConfiguration))
                        ->setName($methodName)
                );
            }
        }

        return $client;
    }

    /**
     * @param $clientName
     * @return AbstractClient
     * @throws InvalidClientClassException
     */
    private function instantiate($clientName)
    {
        $clientClassName = $this->getClassName($clientName);
        $client          = new $clientClassName();

        if (!$client instanceof AbstractClient) {
            throw new InvalidClientClassException(
                sprintf(
                    "%s is not a valid PETL Client class.",
                    $clientClassName
                )
            );
        }
        $client->setName($clientName);

        return $client;
    }
}