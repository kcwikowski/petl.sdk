<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Result;

use PETL\Sdk\Result;

class FlowMonitor extends Result
{
    /**
     * @var string
     */
    protected $id;
    /**
     * @var string
     */
    protected $flowId;
    /**
     * @var string
     */
    protected $parentId;
    /**
     * @var string
     */
    protected $rootId;
    /**
     * @var string
     */
    protected $presetName;
    /**
     * @var int
     */
    protected $status;
    /**
     * @var bool
     */
    protected $isValid = 0;
    /**
     * @var bool
     */
    protected $billed = 0;
    /**
     * @var bool
     */
    protected $stickyOutput = 0;
    /**
     * @var bool
     */
    protected $stickyResult = 0;
    /**
     * @var int
     */
    protected $complexityScore = 0;
    /**
     * @var string
     */
    protected $dateCreated;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return static
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getFlowId()
    {
        return $this->flowId;
    }

    /**
     * @param string $flowId
     * @return static
     */
    public function setFlowId($flowId)
    {
        $this->flowId = $flowId;

        return $this;
    }

    /**
     * @return string
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param string $parentId
     * @return static
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * @return string
     */
    public function getRootId()
    {
        return $this->rootId;
    }

    /**
     * @param string $rootId
     * @return static
     */
    public function setRootId($rootId)
    {
        $this->rootId = $rootId;

        return $this;
    }

    /**
     * @return string
     */
    public function getPresetName()
    {
        return $this->presetName;
    }

    /**
     * @param string $presetName
     * @return static
     */
    public function setPresetName($presetName)
    {
        $this->presetName = $presetName;

        return $this;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return static
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return $this->isValid;
    }

    /**
     * @param bool $isValid
     * @return static
     */
    public function setIsValid($isValid)
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * @return bool
     */
    public function isBilled()
    {
        return $this->billed;
    }

    /**
     * @param bool $billed
     * @return static
     */
    public function setBilled($billed)
    {
        $this->billed = $billed;

        return $this;
    }

    /**
     * @return bool
     */
    public function isStickyOutput()
    {
        return $this->stickyOutput;
    }

    /**
     * @param bool $stickyOutput
     * @return static
     */
    public function setStickyOutput($stickyOutput)
    {
        $this->stickyOutput = $stickyOutput;

        return $this;
    }

    /**
     * @return bool
     */
    public function isStickyResult()
    {
        return $this->stickyResult;
    }

    /**
     * @param bool $stickyResult
     * @return static
     */
    public function setStickyResult($stickyResult)
    {
        $this->stickyResult = $stickyResult;

        return $this;
    }

    /**
     * @return int
     */
    public function getComplexityScore()
    {
        return $this->complexityScore;
    }

    /**
     * @param int $complexityScore
     * @return static
     */
    public function setComplexityScore($complexityScore)
    {
        $this->complexityScore = $complexityScore;

        return $this;
    }

    /**
     * @return string
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param string $dateCreated
     * @return static
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }
}