<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Result;

use PETL\Sdk\Result;

class Metric extends Result
{
    /**
     * @var int
     */
    protected $instances;
    /**
     * @var int
     */
    protected $completed;
    /**
     * @var int
     */
    protected $failed;
    /**
     * @var int
     */
    protected $empty;
    /**
     * @var float
     */
    protected $complexityScore;
    /**
     * @var int
     */
    protected $total;
    /**
     * @var int
     */
    protected $valid;
    /**
     * @var int
     */
    protected $rejected;

    /**
     * @return int
     */
    public function getInstances()
    {
        return $this->instances;
    }

    /**
     * @param int $instances
     * @return static
     */
    public function setInstances($instances)
    {
        $this->instances = $instances;

        return $this;
    }

    /**
     * @return int
     */
    public function getCompleted()
    {
        return $this->completed;
    }

    /**
     * @param int $completed
     * @return static
     */
    public function setCompleted($completed)
    {
        $this->completed = $completed;

        return $this;
    }

    /**
     * @return int
     */
    public function getFailed()
    {
        return $this->failed;
    }

    /**
     * @param int $failed
     * @return static
     */
    public function setFailed($failed)
    {
        $this->failed = $failed;

        return $this;
    }

    /**
     * @return int
     */
    public function getEmpty()
    {
        return $this->empty;
    }

    /**
     * @param int $empty
     * @return static
     */
    public function setEmpty($empty)
    {
        $this->empty = $empty;

        return $this;
    }

    /**
     * @return float
     */
    public function getComplexityScore()
    {
        return $this->complexityScore;
    }

    /**
     * @param float $complexityScore
     * @return static
     */
    public function setComplexityScore($complexityScore)
    {
        $this->complexityScore = $complexityScore;

        return $this;
    }

    /**
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param int $total
     * @return static
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return int
     */
    public function getValid()
    {
        return $this->valid;
    }

    /**
     * @param int $valid
     * @return static
     */
    public function setValid($valid)
    {
        $this->valid = $valid;

        return $this;
    }

    /**
     * @return int
     */
    public function getRejected()
    {
        return $this->rejected;
    }

    /**
     * @param int $rejected
     * @return static
     */
    public function setRejected($rejected)
    {
        $this->rejected = $rejected;

        return $this;
    }
}