<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk\Result\FlowManager;

use PETL\Sdk\Result;

class Start extends Result
{
    /**
     * @var array
     */
    protected $instance;
    /**
     * @var string
     */
    protected $url;
    /**
     * @var bool
     */
    protected $queued;

    /**
     * @return array
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * @param array $instance
     * @return static
     */
    public function setInstance($instance)
    {
        $this->instance = $instance;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return static
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return bool
     */
    public function isQueued()
    {
        return $this->queued;
    }

    /**
     * @param bool $queued
     * @return static
     */
    public function setQueued($queued)
    {
        $this->queued = $queued;

        return $this;
    }
}