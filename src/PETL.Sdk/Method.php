<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk;

use PETL\Standard\Common\Configuration;

class Method
{
    const TYPE_INT      = 'int';
    const TYPE_STRING   = 'string';
    const TYPE_FLOAT    = 'float';
    const TYPE_DATE     = 'date';
    const TYPE_TIME_TAG = 'timeTag';
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $route;
    /**
     * @var string
     */
    protected $method;
    /**
     * @var array
     */
    protected $inputs;
    /**
     * @var array
     */
    protected $parameters;
    /**
     * @var string
     */
    protected $resultClass;

    /**
     * Method constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        Configuration::apply($this, $options);
    }

    /**
     * @param array $inputs
     * @return array|bool
     */
    public function validateInputs(array $inputs)
    {
        $inputs =
            array_intersect_key(
                $inputs,
                array_filter(
                    $this->getInputs() + [Input::PROPERTY_ACCOUNT => 'string']
                )
            );

        foreach ($inputs as $name => $value) {
            if (array_key_exists($name, $this->getInputs())) {
                $type = $this->getInputs()[$name];
                if (stripos($type, '\\') === false) {
                    if (gettype($value) !== $type) {
                        return false;
                    }
                } else {
                    if (!$value instanceof $type) {
                        return false;
                    }
                }
            }
        }

        return $inputs;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param string $route
     * @return self
     */
    public function setRoute($route)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return self
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return array
     */
    public function getInputs()
    {
        return $this->inputs;
    }

    /**
     * @param array $inputs
     * @return self
     */
    public function setInputs($inputs)
    {
        $this->inputs = $inputs;

        return $this;
    }

    /**
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     * @return self
     */
    public function setParameters($parameters)
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @return string
     */
    public function getResultClass()
    {
        return $this->resultClass;
    }

    /**
     * @param string $resultClass
     * @return self
     */
    public function setResultClass($resultClass)
    {
        $this->resultClass = $resultClass;

        return $this;
    }
}