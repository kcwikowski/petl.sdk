<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk;

class InvalidMethodException extends Exception
{
    /**
     * @param string $uri
     * @return $this
     */
    public function appendUri($uri)
    {
        $this->message .= '(' . $uri . ')';
        return $this;
    }
}