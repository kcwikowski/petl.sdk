<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk;

use PETL\Standard\Authentication\KSCBlock;
use PETL\Standard\Common\Configuration;

/**
 * Class ClientConfiguration
 * @package PETL\Sdk
 */
class ClientConfiguration
{
    /**
     * @var string
     */
    protected $endpoint;
    /**
     * @var KSCBlock
     */
    protected $ksc;
    /**
     * @var array
     */
    protected $methods;
    /**
     * @var string
     */
    protected $routeBase;
    /**
     * @var string
     */
    protected $resultClass;

    /**
     * ClientConfiguration constructor.
     * @param array $configuration
     */
    public function __construct(array $configuration = [])
    {
        Configuration::apply($this, $configuration);
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     * @return self
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * @return array
     */
    public function getMethods()
    {
        return $this->methods;
    }

    /**
     * @param array $methods
     * @return self
     */
    public function setMethods($methods)
    {
        $this->methods = $methods;

        return $this;
    }

    /**
     * @return KSCBlock
     */
    public function getKsc()
    {
        return $this->ksc;
    }

    /**
     * @param KSCBlock $ksc
     * @return self
     */
    public function setKsc($ksc)
    {
        $this->ksc = $ksc;

        return $this;
    }

    /**
     * @return string
     */
    public function getRouteBase()
    {
        return $this->routeBase;
    }

    /**
     * @param string $routeBase
     * @return static
     */
    public function setRouteBase($routeBase)
    {
        $this->routeBase = $routeBase;

        return $this;
    }

    /**
     * @return string
     */
    public function getResultClass()
    {
        return $this->resultClass;
    }

    /**
     * @param string $resultClass
     * @return static
     */
    public function setResultClass($resultClass)
    {
        $this->resultClass = $resultClass;

        return $this;
    }
}