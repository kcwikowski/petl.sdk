<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk;

use PETL\Standard\Authentication\IdentityAwareTrait;
use PETL\Standard\Authentication\KSCBlockAwareInterface;
use PETL\Standard\Authentication\KSCBlockAwareTrait;
use PETL\Standard\Common\Configuration;

/**
 * Class AuthConfiguration
 * @package PETL\Sdk
 */
class AuthConfiguration implements KSCBlockAwareInterface
{
    use KSCBlockAwareTrait, IdentityAwareTrait;

    /**
     * ClientConfiguration constructor.
     * @param array $configuration
     */
    public function __construct(array $configuration = [])
    {
        Configuration::apply($this, $configuration);
    }
}