<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk;

use PETL\Standard\Common\Configuration;

abstract class Input implements \JsonSerializable
{
    const PROPERTY_ACCOUNT = 'account';
    /**
     * @var string
     */
    protected $account = '';

    /**
     * Input constructor.
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        if ($options) {
            Configuration::apply($this, $options);
        }
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return
            get_object_vars($this);
    }

    /**
     * @return bool
     */
    public abstract function validate();

    /**
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param string $account
     * @return static
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }
}