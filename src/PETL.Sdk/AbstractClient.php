<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk;

use PETL\Sdk\Authentication\ForbiddenException;
use PETL\Sdk\Authentication\KeyGenerator;
use PETL\Sdk\Authentication\KeyGeneratorInterface;
use PETL\Sdk\Authentication\Stateless;
use PETL\Sdk\Authentication\UnauthorizedException;
use PETL\Standard\Authentication\SecureData;
use PETL\Standard\Common\Configuration;
use PETL\Standard\Common\MessengerInterface;
use PETL\Standard\Common\MessengerTrait;
use PETL\Standard\Symbols;
use Zend\Http\Client;
use Zend\Http\Response;

/**
 * Class AbstractClient
 * @package PETL\Sdk
 */
abstract class AbstractClient implements MessengerInterface
{
    use MessengerTrait;
    /**
     * @var string
     */
    protected $name;
    /**
     * @var string
     */
    protected $endpoint;
    /**
     * @var string
     */
    protected $routeBase;
    /**
     * @var Method[]
     */
    protected $methods;
    /**
     * @var Client
     */
    protected $httpClient;
    /**
     * @var Stateless
     */
    protected $auth;
    /**
     * @var string
     */
    protected $resultClass;
    /**
     * @var bool
     */
    protected $rawOutput;

    /**
     * @param $methodName
     * @param array $arguments
     * @return array|Result
     * @throws InvalidInputException
     */
    public function __call($methodName, array $arguments)
    {
        $inputs =
            $arguments ? reset($arguments) : [];

        if (!(
            is_array($inputs)
            || $inputs instanceof Input
        )) {
            throw new InvalidInputException(
                sprintf(
                    "Input for %s:%s method must be either an array or instance of %s.\n Input provided: %s",
                    $this->getName(),
                    $methodName,
                    Input::class,
                    json_encode($inputs)
                )
            );
        }

        return
            $this->callMethod(
                $methodName,
                is_array($inputs) ?
                    $inputs :
                    $inputs->jsonSerialize()
            );
    }

    /**
     * @return Method[]
     */
    public function &getMethods()
    {
        if (is_null($this->methods)) {
            $this->methods = [];
        }

        return $this->methods;
    }

    /**
     * @param Method $method
     * @return $this
     */
    public function addMethod(Method $method)
    {
        $this->getMethods()[$method->getName()] = $method;

        return $this;
    }

    /**
     * @param Method[] $methods
     * @return static
     */
    public function setMethods(array $methods)
    {
        $this->methods = &$methods;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return static
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     * @return static
     */
    public function setEndpoint($endpoint)
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * @return Client
     */
    public function getHttpClient()
    {
        if (is_null($this->httpClient)) {
            $this->httpClient =
                new Client(
                    null,
                    $this->getHttpClientOptions()
                );
        }

        return $this->httpClient;
    }

    /**
     * @return Stateless
     */
    public function getAuth()
    {
        return $this->auth;
    }

    /**
     * @param Stateless $auth
     * @return static
     */
    public function setAuth($auth)
    {
        $this->auth = $auth;

        return $this;
    }

    /**
     * @return string
     */
    public function getResultClass()
    {
        return $this->resultClass;
    }

    /**
     * @param string $resultClass
     * @return static
     */
    public function setResultClass($resultClass)
    {
        $this->resultClass = $resultClass;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRawOutput()
    {
        return $this->rawOutput;
    }

    /**
     * @param bool $rawOutput
     * @return static
     */
    public function setRawOutput($rawOutput)
    {
        $this->rawOutput = $rawOutput;

        return $this;
    }

    /**
     * @return string
     */
    public function getRouteBase()
    {
        return $this->routeBase;
    }

    /**
     * @param string $routeBase
     * @return static
     */
    public function setRouteBase($routeBase)
    {
        $this->routeBase = $routeBase;

        return $this;
    }

    /**
     * @param Method $method
     * @return string
     */
    protected function getMethodUri(Method $method)
    {
        return
            $this->getEndpoint() . $this->getRouteBase() . $method->getRoute();
    }

    /**
     * @return array
     */
    protected function getHttpClientOptions()
    {

        return
            [
                'adapter'     => 'Zend\Http\Client\Adapter\Curl',
                'curloptions' => [
                    CURLOPT_FOLLOWLOCATION    => true,
                    CURLOPT_TIMEOUT_MS        => 2150,
                    CURLOPT_CONNECTTIMEOUT_MS => 2000,
                ],
            ];
    }

    /**
     * @param $methodName
     * @param array $inputs
     * @param array $spanTree
     * @return array|Result
     * @throws InvalidInputException
     * @throws InvalidMethodException
     * @throws MethodNotFoundException
     */
    private function callMethod($methodName, array $inputs = [], array $spanTree = [])
    {
        if (!array_key_exists($methodName, $this->getMethods())) {
            throw new MethodNotFoundException(
                sprintf(
                    "%s client does not provide '%s' method",
                    ucwords($this->getName()),
                    $methodName
                )
            );
        }

        $method = $this->getMethods()[$methodName];
        $inputs = $method->validateInputs($inputs);

        if (false === $inputs) {
            throw new InvalidInputException(
                sprintf(
                    "%s:%s.\nInvalid method input.\nInput provided: %s\nInput expected: %s",
                    $this->getName(),
                    $methodName,
                    json_encode($inputs),
                    json_encode($method->getInputs())
                )
            );
        }

        $spanTree[$method->getName()] = true;
        $inputs                       = $inputs + ($method->getParameters() ?: []);

        if ($method->getMethod()) {
            if (array_key_exists($method->getMethod(), $spanTree)) {

                throw new InvalidMethodException(
                    $method->getName() . ':' . $method->getMethod()
                );
            }

            return
                $this->callMethod(
                    $method->getMethod(),
                    $inputs,
                    $spanTree
                );
        } else {

            return
                $this->makeHttpCall(
                    $method,
                    $inputs
                );
        }
    }

    /**
     * @return array
     */
    private function getHeaders()
    {
        $seed      = KeyGenerator::generateRandomString(8, KeyGeneratorInterface::MODE_PRINTABLE);
        $hash      = sha1($seed);
        $signature = $seed . ';' . $hash;

        return
            [
                Symbols::SDK_HTTP_HEADER => $signature,
                "X-Requested-With"       => "XMLHttpRequest",
                "Accept"                 => "application/json, text/javascript, */*; q=0.01",
            ];
    }

    /**
     * @param array $inputs
     * @return array
     */
    private function getPost(array $inputs)
    {
        return
            [
                Symbols::SDK_POST_FIELD =>
                    json_encode(
                        $this->getAuth()->sign($inputs)
                    ),
            ];
    }

    /**
     * @param Method $method
     * @param array $inputs
     * @return array|Result
     * @throws InvalidMethodException
     */
    private function makeHttpCall(Method $method, array $inputs)
    {
        $uri      = $this->getMethodUri($method);
        $response =
            $this->getHttpClient()
                ->setUri(
                    $uri
                )
                ->setMethod('POST')
                ->setParameterPost(
                    $this->getPost($inputs)
                )
                ->setHeaders(
                    $this->getHeaders()
                )
                ->send();

        try {
            return
                $this->parseResponse(
                    $response,
                    $method
                );
        } catch (InvalidMethodException $e) {
            throw $e->appendUri($uri);
        }
    }

    /**
     * @param Response $response
     * @param Method $method
     * @return array|mixed
     * @throws BadRequestException
     * @throws BadResponseException
     * @throws ForbiddenException
     * @throws InvalidMethodException
     * @throws UnauthorizedException
     */
    private function parseResponse(Response $response, Method $method)
    {
        $result      = [];
        $resultClass = false;

        switch ($response->getStatusCode()) {
            case Response::STATUS_CODE_200:
                $valid = false;
                try {

                    if ($this->getAuth()->verify(
                        new SecureData(
                            json_decode(
                                $response->getBody(),
                                true
                            )
                        ),
                        $result
                    )) {
                        $valid = true;
                    }
                } finally {
                    if (!$valid) {
                        throw (new BadResponseException('Response validation has failed.'))->setResponseBody($response->getBody());
                    }
                }
                break;
            case Response::STATUS_CODE_400:
                throw new BadRequestException();
                break;
            case Response::STATUS_CODE_401:
                throw new UnauthorizedException(
                    "Failed to authenticate. Please check SDK Client credentials configuration."
                );
                break;
            case Response::STATUS_CODE_403:
                throw new ForbiddenException(
                    sprintf(
                        "You do not have access to method %s:%s.",
                        $this->getName(),
                        $method->getName()
                    )
                );
                break;
            case Response::STATUS_CODE_404:
                throw new InvalidMethodException(
                    sprintf(
                        "Server could not find requested method: %s:%s",
                        $this->getName(),
                        $method->getName()
                    )
                );
                break;

            default:
                break;
        }

        switch (true) {
            case $this->isRawOutput():
                break;
            case $method->getResultClass():
                $resultClass = $method->getResultClass();
                break;
            case $this->getResultClass():
                $resultClass = $this->getResultClass();
                break;
            default:
                $resultClass = Result::class;
                break;
        }

        if ($resultClass) {
            $result =
                Configuration::apply(
                    new $resultClass(),
                    $result
                );
        }

        return $result;
    }
}