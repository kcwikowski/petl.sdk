<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

return [
    'PETL.Sdk' => [
        'version' => '0.4.0',
        'Factory' => [
            'endpoint'        => 'https://petl.io',
            'authOptionsPath' => 'PETL.Sdk:Authentication',
            'classPrefix'     => 'PETL.Sdk.Client.',
            'clients'         => [
                'FlowStat'    => [
                    'routeBase'   => '/flow-stat',
                    'resultClass' => \PETL\Sdk\Result\Metric::class,
                    'methods'     => [
                        'metric'     => [
                            'route'  => '/metric',
                            'inputs' => [
                                'type'    => 'string',
                                'timeTag' => 'string',
                            ],
                        ],
                        'flowMetric' => [
                            'route'  => '/flow-metric',
                            'inputs' => [
                                'flow'    => 'string',
                                'type'    => 'string',
                                'timeTag' => 'string',
                            ],
                        ],
                    ],
                ],
                'FlowManager' => [
                    'routeBase' => '/flow-manager',
                    'methods'   => [
                        'start' => [
                            'route'       => '/start',
                            'resultClass' => \PETL\Sdk\Result\FlowManager\Start::class,
                            'inputs'      => [
                                'flow'             => 'string',
                                'options'          => 'array',
                                'preset'           => 'string',
                                'region'           => 'string',
                                'executionContext' => 'string',
                            ],
                        ],
                    ],
                ],
                'FlowMonitor' => [
                    'routeBase'   => '/flow-manager',
                    'resultClass' => \PETL\Sdk\Result\FlowMonitor::class,
                    'methods'     => [
                        'info' => [
                            'route'  => '/instance/info',
                            'inputs' => [
                                'flow'         => 'string',
                                'flowInstance' => 'string',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
];