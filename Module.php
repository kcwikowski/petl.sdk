<?php
/**
 * Copyright (c) 2019.
 * Intellectual property of KCI Data Ltd.
 */

namespace PETL\Sdk;

class Module
{
    public function getServiceConfig()
    {
        return [
            'abstract_factories' => [
                ClientFactory::class,
            ],
        ];
    }

    public function getConfig()
    {

        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {

        return [
            'Zend\Loader\StandardAutoloader' => [
                'namespaces' => [
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ],
            ],
        ];
    }
}
